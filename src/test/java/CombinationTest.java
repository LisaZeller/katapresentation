
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CombinationTest {
    @Test
    public void tests() {
        Assert.assertEquals(8, CombinationLock.countLetters("a b c d e fgh"));
        assertEquals(11, CombinationLock.countLetters("one two three"));
        assertEquals(11, CombinationLock.countLetters("red blue grey"));
        assertEquals(30, CombinationLock.countLetters("house city garden husband wife door"));
        assertEquals(24, CombinationLock.countLetters("this is with numbers 1234567"));
        assertEquals(11, CombinationLock.countLetters("justoneword"));
    }
}
