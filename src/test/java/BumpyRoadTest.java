import org.junit.Assert;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

public class BumpyRoadTest {

    @Test
    public void Test() {
        Assert.assertEquals("Woohoo!", BumpyRoad.bumps("n"));
        assertEquals( "Car Dead",BumpyRoad.bumps("_nnnnnnn_n__n______nn__nn_nnn"));
        assertEquals("Woohoo!", BumpyRoad.bumps("______n___n_"));
        assertEquals("Car Dead", BumpyRoad.bumps("nnnnnnnnnnnnnnnnnnnnn"));
        assertEquals("Woohoo!", BumpyRoad.bumps("___________________"));
        assertEquals("Woohoo!", BumpyRoad.bumps("nnnnnnnnnnnnnnn"));
        assertEquals("Car Dead", BumpyRoad.bumps("nnnnnnnnnnnnnnnn"));

    }
}
