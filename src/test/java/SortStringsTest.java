import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SortStringsTest {

    @Test
    public void tests() {
        Assert.assertEquals("sentence,short", SortStrings.sortString("This is a short sentence"));
        assertEquals("details,information,longer", SortStrings.sortString("This is a longer sentence with more details and information"));
        assertEquals("", SortStrings.sortString("No word with more than four"));
        assertEquals("could,tests,write", SortStrings.sortString("You could write your own tests here"));

    }
}
